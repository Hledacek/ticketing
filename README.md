# Improved Stephen Grider's ticketing app

# AWS CLI commands

Create a key pair
$ aws ec2 create-key-pair --key-name tf-ec2-key --region us-east-1 --query 'KeyMaterial' --output text > tf-ec2-key.pem

# Terraform commands

$ terraform apply --var-file=prod.tfvars

# Terraform addons

Blast Radius
Terragrunt
