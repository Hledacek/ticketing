provider "aws" {
  alias = "aws-east"
  region = "us-east-1"
}

resource "aws_s3_bucket" "s3_state_bucket" {
  bucket = "tf-remote-state-bucket-84875"
}

terraform {
  backend "s3" {
    bucket = "tf-remote-state-bucket-84875"
    key = "aws_tf_remote_state.tfstates"
    region = "us-east-1"
  }
}

data "aws_caller_identity" "my_account" {}

module "vpc-networking" {
  source    = "./modules/vpc-networking"
}

data "aws_ami" "ubuntu_latest" {
  owners = ["099720109477"]
  most_recent = true

  filter {
    name = "virtualization-type"
    values = ["hvm"]
  }
}

resource "aws_instance" "tf-main-instance" {
  ami = data.aws_ami.ubuntu_latest.id
  instance_type = var.ec2_instance_type
  key_name = var.ec2_keypair
  security_groups = [ aws_security_group.ec2-security-group.id ]
  subnet_id = "${module.vpc-networking.public_subnet_1_id}"
}

resource "aws_security_group" "ec2-security-group" {
  name = "EC2-Instance-SG"
  vpc_id = "${module.vpc-networking.vpc_id}"
  
  ingress {
    description      = "Allow ingress"
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }

  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }

  tags = {
    Name = "EC2-Security-Group"
  }
}