variable "ec2_keypair"  {
  default = "tf-ec2-key"
}

variable "ec2_instance_type" {
  default = "t2.micro"
}