variable "region" {
  default = "us-east-1"
}

variable "vpc_cidr_block" {
  default = "10.0.0.0/16"
  description = "VPC CIDR block"
}

variable "public_subnet_1_cidr_block" {
  default = "10.0.1.0/24"
  description = "Public Subnet 1 CIDR block"
}

variable "public_subnet_2_cidr_block" {
  default = "10.0.2.0/24"
  description = "Public Subnet 2 CIDR block"
}

variable "public_subnet_3_cidr_block" {
  default = "10.0.3.0/24"
  description = "Public Subnet 3 CIDR block"
}

variable "private_subnet_1_cidr_block" {
  default = "10.0.10.0/24"
  description = "Private Subnet 1 CIDR block"
}


variable "private_subnet_2_cidr_block" {
  default = "10.0.11.0/24"
  description = "Private Subnet 2 CIDR block"
}


variable "private_subnet_3_cidr_block" {
  default = "10.0.12.0/24"
  description = "Private Subnet 3 CIDR block"
}

variable "eip_association_address" {
  default = "10.0.3.0/24" // Public Subnet 3
}

