provider "aws" {
  region = var.region
}

resource "aws_vpc" "module_vpc" {
  cidr_block = var.vpc_cidr_block
  enable_dns_hostnames = true

  tags = {
    "Name" = "Production-VPC"
  }
}

resource "aws_subnet" "public_subnet_1" {
  vpc_id = aws_vpc.module_vpc.id
  cidr_block = var.public_subnet_1_cidr_block
  availability_zone = "${var.region}a"

  tags = {
    "Name" = "Public-Subnet-1"
  }
}

resource "aws_subnet" "public_subnet_2" {
  vpc_id = aws_vpc.module_vpc.id
  cidr_block = var.public_subnet_2_cidr_block
  availability_zone = "${var.region}b"

  tags = {
    "Name" = "Public-Subnet-2"
  }
}

resource "aws_subnet" "public_subnet_3" {
  vpc_id = aws_vpc.module_vpc.id
  cidr_block = var.public_subnet_3_cidr_block
  availability_zone = "${var.region}c"

  tags = {
    "Name" = "Public-Subnet-3"
  }
}

resource "aws_subnet" "private_subnet_1" {
  vpc_id = aws_vpc.module_vpc.id
  cidr_block = var.private_subnet_1_cidr_block

  availability_zone = "${var.region}a"

  tags = {
    "Name" = "Private-Subnet-1"
  }
}

resource "aws_subnet" "private_subnet_2" {
  vpc_id = aws_vpc.module_vpc.id
  cidr_block = var.private_subnet_2_cidr_block

  availability_zone = "${var.region}b"

  tags = {
    "Name" = "Private-Subnet-2"
  }
}

resource "aws_subnet" "private_subnet_3" {
  vpc_id = aws_vpc.module_vpc.id
  cidr_block = var.private_subnet_3_cidr_block

  availability_zone = "${var.region}c"

  tags = {
    "Name" = "Private-Subnet-3"
  }
}

resource "aws_route_table" "public_route_table" {
  vpc_id = aws_vpc.module_vpc.id
  tags = {
    "Name" = "Public Route Table"
  }
}

resource "aws_route_table" "private_route_table" {
  vpc_id = aws_vpc.module_vpc.id
  tags = {
    "Name" = "Private Route Table"
  }
}

resource "aws_route_table_association" "public_subnet_1_association" {
  route_table_id = aws_route_table.public_route_table.id
  subnet_id = aws_subnet.public_subnet_1.id
}

resource "aws_route_table_association" "public_subnet_2_association" {
  route_table_id = aws_route_table.public_route_table.id
  subnet_id = aws_subnet.public_subnet_2.id
}

resource "aws_route_table_association" "public_subnet_3_association" {
  route_table_id = aws_route_table.public_route_table.id
  subnet_id = aws_subnet.public_subnet_3.id
}

resource "aws_route_table_association" "private_subnet_1_association" {
  route_table_id = aws_route_table.private_route_table.id
  subnet_id = aws_subnet.private_subnet_1.id
}

resource "aws_route_table_association" "private_subnet_2_association" {
  route_table_id = aws_route_table.private_route_table.id
  subnet_id = aws_subnet.private_subnet_2.id
}

resource "aws_route_table_association" "private_subnet_3_association" {
  route_table_id = aws_route_table.private_route_table.id
  subnet_id = aws_subnet.private_subnet_3.id
}

resource "aws_eip" "elastic_ip_for_nat_gw" {
  vpc = true
  associate_with_private_ip = var.eip_association_address

  tags = {
    "Name" = "Production-EIP"
  }
}

resource "aws_nat_gateway" "nat_gateway" {
  allocation_id = aws_eip.elastic_ip_for_nat_gw.id
  subnet_id = aws_subnet.public_subnet_1.id

  tags = {
    "Name" = "Production-NAT-GW"
  }
}

resource "aws_route" "nat-gateway_route" {
  route_table_id = aws_route_table.private_route_table.id // or public
  nat_gateway_id = aws_nat_gateway.nat_gateway.id
  destination_cidr_block = "0.0.0.0/0"
}

resource "aws_internet_gateway" "internet_gateway" {
  vpc_id = aws_vpc.module_vpc.id

  tags = {
    "Name" = "Production-IGW"
  }
}

resource "aws_route" "igw_route" {
  route_table_id = aws_route_table.public_route_table.id
  gateway_id = aws_internet_gateway.internet_gateway.id
  destination_cidr_block = "0.0.0.0/0"
}